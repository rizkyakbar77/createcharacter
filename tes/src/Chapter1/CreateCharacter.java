package Chapter1;
import java.util.Scanner;
import java.util.Random;

public class CreateCharacter {

    public static void main(String[] args) {
        boolean cek = true;
        Scanner scan = new Scanner(System.in);
        Random acak = new Random();
        String[] karakter = new String[4];
        int[] atribut = new int[9];
        char pilihRas, pilihKelas;

        while(cek){


         // Pembukaan
         System.out.println("==========================================================");
         System.out.println("Welcome to Terra , Please create your character ");
         System.out.print("Press any button to continue !");
         String tekan = scan.nextLine();
         System.out.println("==========================================================\n");

         // Pembuatan Username
         System.out.print("Username : ");
         karakter[0] = scan.nextLine();
         atribut[0] = 1;  // Level
         atribut[1] = acak.nextInt(10); // Stat Strength
         atribut[2] = acak.nextInt(10); // Stat Agility
         atribut[3] = acak.nextInt(10); // Stat Vitality
         atribut[4] = acak.nextInt(10); // Stat Intelligence
         atribut[5] = acak.nextInt(10);  // Stat Dexterity
         atribut[6] = acak.nextInt(10);  // Stat Luck

         // Pemilihan Ras
         System.out.println("==========================================================");
         System.out.println("Choose your Race ! ");
         System.out.println("1. Human  ");  // default
         System.out.println("2. Elf ");
         System.out.println("3. Half Orc ");
         System.out.println("4. Gnome ");
         System.out.println("==========================================================\n");
         pilihRas = scan.next().charAt(0);

         switch (pilihRas) {
             case '2' -> {
                 karakter[1] = "Elf";
                 atribut[1] += 6;
                 atribut[2] += 9;
                 atribut[3] += 8;
                 atribut[4] += 9;
                 atribut[5] += 12;

             }
             case '3' -> {
                 karakter[1] = "Half Orc";
                 atribut[1] += 15;
                 atribut[2] += 6;
                 atribut[3] += 12;
                 atribut[4] += 5;
                 atribut[5] += 6;
             }
             case '4' -> {
                 karakter[1] = "Gnome";
                 atribut[1] += 5;
                 atribut[2] += 10;
                 atribut[3] += 8;
                 atribut[4] += 14;
                 atribut[5] += 7;
             }
             default -> {
                 karakter[1] = "Human";
                 atribut[1] += 10;
                 atribut[2] += 8;
                 atribut[3] += 10;
                 atribut[4] += 9;
                 atribut[5] += 7;
             }
         }

         // Pemilihan Job Class
         System.out.println("==========================================================");
         System.out.println("Choose your Job Class ! ");
         System.out.println("1. Warrior  "); // default
         System.out.println("2. Archer ");
         System.out.println("3. Mage ");
         System.out.println("4. Rogue ");
         System.out.println("==========================================================\n");
         pilihKelas = scan.next().charAt(0);

         switch (pilihKelas) {
             case '2' -> {
                 karakter[2] = "Archer";
                 atribut[1] += 5;
                 atribut[5] += 10;
                 karakter[3] = "Bow Mastery , Aiming , Quickshot";
             }
             case '3' -> {
                 karakter[2] = "Mage";
                 atribut[4] += 15;
                 karakter[3] = "Mana Control , Mana Shield , Mana Arrow";
             }
             case '4' -> {
                 karakter[2] = "Rogue";
                 atribut[2] += 10;
                 atribut[5] += 5;
                 karakter[3] = "Dagger Mastery , Stealth , Lockpicking";
             }
             default  -> {
                 karakter[2] = "Warrior";
                 atribut[1] += 8;
                 atribut[3] += 7;
                 karakter[3] = "Sword Mastery , Bash , Slash ";
             }
         }

         // Set Hit points dan Mana points
         atribut[7] = (atribut[0]*50) + (atribut[3]*30);
         atribut[8] = (atribut[0]*20) + (atribut[4]*25);

         // Penutup
         System.out.println("==========================================================");
         System.out.println("Congrats! Your character have been created "+"\n");
         System.out.println("Characters Detail !");
         detailKarakter(karakter,atribut);
         System.out.println("==========================================================");

         System.out.println("Are you satisfied with your character!");
         System.out.println("1. Yes");
         System.out.println("2. No , Recreate my character");
         int puas = scan.nextInt();
         if(puas == 1){
             cek = false;
         }

     }
    }

    public static void detailKarakter(String[] karakter , int[] atribut){
        System.out.println("Username   : " + karakter[0]);
        System.out.println("Level      : "+ atribut[0]);
        System.out.println("Race       : "+ karakter[1]);
        System.out.println("Job Class  : "+ karakter[2]);
        System.out.println("HP         : "+ atribut[7]);
        System.out.println("MP         : "+ atribut[8]);
        System.out.println("Str        : " + atribut[1]);
        System.out.println("Agi        : "+ atribut[2]);
        System.out.println("Vit        : "+ atribut[3]);
        System.out.println("Int        : "+ atribut[4]);
        System.out.println("Dex        : "+ atribut[5]);
        System.out.println("Luk        : "+ atribut[6]);
        System.out.println("Skill      : "+ karakter[3]);
    }



}